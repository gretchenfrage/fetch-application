
## Phoenix Kahlo Fetch Rewards Data Engineering Application Take-Home Project

I developed this project in Rust.

## Running Instructions

### Installing Rust

Rust is usually very easy to install. See https://rustup.rs/ for an operating
system-specific one-liner to install the Rust toolchain on your system.

If your system is new and/or not used to compiled languages, you may also need
to install packages such as gcc and libssl-dev.

### Running this program

Before running this program, use `docker compose up` as normal to start the
provided postgresql and localstack containers. The only thing I changed was
binding the postgres server to host port 5433, because I'm already using 5432
for something.

Before running this program:

```sh
# enter the source directory
cd pseudonymizer
# set this environment variable to tell the AWS client to connect to localstack
# and not try to find credentials. otherwise it'll just to find regular AWS
# credentials for connecting to the real AWS.
export LOCALSTACK_URL=http://localhost:4566/
```

To run this program:

```sh
# use cargo run build and run this program, with command line arguments
cargo run -- http://localhost:4566/000000000000/login-queue postgresql://postgres:postgres@localhost:5433
```

This will read all messages from the SQS queue, pseudonymize them, and write
the outputs to the SQL database. To view the SQL database after:

1. `psql -d postgres -U postgres -p 5433 -h localhost -W`
2. password = `postgres`
3. `select * from user_logins;`

Example output:

```
               user_id                | device_type |    masked_ip    | masked_device_id | locale | app_version | create_date 
--------------------------------------+-------------+-----------------+------------------+--------+-------------+-------------
 424cdd21-063a-43a7-b91b-7ca1a833afae | android     | 211.78.138.241  | 034-81-6336      | RU     |         230 | 1970-01-20
 c0173198-76a8-4e67-bfc2-74eaa3bbff57 | ios         | 176.183.173.34  | 396-27-8802      | PH     |          26 | 1970-01-20
 66e0635b-ce36-4ec7-aa9e-8a8fca9b83d4 | ios         | 72.65.232.93    | 152-35-0382      |        |         221 | 1970-01-20
 181452ad-20c3-4e93-86ad-1934c9248903 | android     | 230.142.237.184 | 203-51-9996      | ID     |          96 | 1970-01-20
 60b9441c-e39d-406f-bba0-c7ff0e0ee07f | android     | 78.215.24.244   | 984-32-6692      | FR     |          46 | 1970-01-20
 5082b1ae-6523-4e3b-a1d8-9750b4407ee8 | android     | 244.251.58.226  | 833-35-6493      |        |          37 | 1970-01-20
 5bc74293-3ca1-4f34-bb89-523887d0cc2f | ios         | 24.98.146.184   | 955-66-4137      | PT     |         228 | 1970-01-20
 92d8ceec-2e12-49f3-81bd-518fe66971ec | android     | 240.173.73.70   | 666-02-6433      | BR     |          55 | 1970-01-20
 05e153b1-4fa1-474c-bd7e-9f74d1c495e7 | android     | 172.194.6.237   | 892-45-8577      |        |          50 | 1970-01-20
(truncated)
 ```

You may also notice this generates a `pseudonymization-secret-key` file,
containing, for example:

```
m/O4NK69227BgMEz5lUeiQW7fjdaEFnm3Zz0dWfF4l8
```

Bonus: you can build and view the API docs:

```sh
cargo doc
```

Then open `target/doc/pseudonymizer/index.html` with your favorite web browser.

## Design Decisions

### Pseudonymization technique

Some basic correctness requirements are:

- the same input value maps to the same pseudonymized value
- different input values are astronomically unlikely to map to the same output
  value

The primary security requirement is that an attacker should not be able to test
whether a given pseudonymized value is the pseudonymized version of a given
input value.

Some additional desirable properties worth considering should be:

- scalable and efficient in terms of computer load and program complexity
- tools which are made to process non-pseudonymized logs can process
  pseudonmized logs

One possible technique would be to maintain a key/value mapping between
original values and pseudonymized values. Upon encounting an input value, if
an entry for it exists in the mapping, the pre-determined corresponding
pseudonymized value would be used, and if not, a unique (sequential? random
with collision detection? many possibilities.) pseudonymized value would be
generated and inserted into the mapping. Pseudonymized values could easily be
generated to be of the same format.

However, if the key/value mapping grows very large, if this program runs on
large logs or perhaps runs continuously as a backend component for an extended
period of time, this key/value mapping may no longer fit in memory. This would
necessitate the use of some sort of key/value database to store this
association, which would degrade performance and introduce additional
complexity.

Another possible technique would be to pseudonymize values by running them
through a keyed hash function, more commonly referred to as a MAC. This would
solve the problem of an ever-growing key/value association by reducing the only
state the pseudonymizer has to maintain to a single unchanging secret key.
However, this is likely not compatible with preserving value format. For
example, if a 32-bit integer is formatted with SHA192, the resultant hash
would not fit within a 32-bit integer, which would risk breaking existing log
processing tools. The hash could be truncated to reduce its size, but this
would violate correctness requirements by raising the chance of a collision.

Instead of either of those, the technique I decided on was **format-preserving
encryption**--particularly, the FF1 algorithm, as specified in Draft NIST
Special Publication 800-38G. This is an encryption algorithm which is capable
of taking a string with a given length and radix and using AES to encrypt it
into a different string with the same length and the same radix. This can be
used very flexibly to construct pseudonymization procedures which act as random
permutations over the spaces of many formats. The resultant procedures thus use
a secret AES key to securely convert input values to pseudonymized output 
values that are still valid for that format.

Comments within this repository's source code contains more details on how this
is done for the particular field formats of this exercise.

### General architecture

This program uses [rusoto](https://github.com/rusoto/rusoto) as a Rust AWS API
client, `tokio-postgres` as a postgresql client, the `fpe` and `aes` crates for
cryptography, and generates `openssl` for secure random secret generation, as
well as other common rust crates.

API credentials are configured with environment variables, as described by
comments in the source code.

Upon running, it reads a base64-encoded AES key from the file
`pseudonymization-secret-key`, generating it if it does not exist, and uses
that as the key in FF1-based procedures to pseudonymize values. That key should
be kepy secure, as it can be used to decrypt and thus deanonymize the
pseudonymized logs.

Pseudonymized logs should be parseable by any program which can parse
non-pseudonymized logs.

## Requested questions

### "How would you deploy this application in production?" + "What other components would you want to add to make this production ready?""

As it exists, this program terminates once the queue is drained. To be deployed
in a production backend environment, it would have to be modified to instead
wait until additional messages become available in the queue and continue
processing them.

I would also want to instrument this with a logging backend that can better
format debug logs for machine processing, and also possibly add some form of
liveness check.

Beyond that, I'd probably set this up as a Kubernetes service. Credentials and
pseudonymization secrets as well as health checks, restarting, scaling, and
exponential backoffs can be handled through standard Kubernetes faculties. It
would act as processes which would consume log entries from the SQS queue,
pseudonymize them using its private key, and post the pseudonymized data to the
postgres database.

It's possible that pseudonymization of multiple types of logs from multiple
different queues would arise as a requirement. In that cases, I would simply
modify this program to spawn a parallel task for each type of log, and use some
monitor task to try to restart them individually if they fail in an exponential
backoff pattern, before eventually aborting the whole process if that doesn't
work to see if Kubernetes could fix it via a contain reboot.

### "How can this application scale with a growing dataset?"

Excellently. Since the only state necessary for pseudonymization is an
unchanging private key, arbitrarily many pseudonymization coprocesses could be
spun up to process log entries in parallel without having to synchronize with
each other and without dealing with a growing key/value data set.

### "How can PII be recovered later on?"

The FF1 encryption algorithm has a decryption counterpart. The functions in
`pseudonymize.rs` could trivially have decryption counterparts written for them
which accept the private key and a pseudonymized value and decrypt it into the
original value.

### "What are the assumptions you made?"

I assumed things including:

- it is worth it to make pseudonymized logs automatically compatible with
  software designed to parse and processes non-pseudonymized logs of the given
  structure and format
- it is worth it for aforementioned reasons to avoid maintaining an
  ever-growing key/value database
- environment variables are suitable for configuring credentials
- it is not necessary to hide whether a user has an IPV4 or IPV6 address
- the `fpe` codebase is sufficiently trustworthy for our purpose
- it is better to error and abort on encountering seemingly invalidly formatted
  pseudonymization-targeted fields than to risk passing them through
  non-pseudonymized
- the Draft NIST Special Publical 800-38G Revision 1 document does not contain
  critical errors
- it is possible and acceptable to write secret keys to the file system
- P != NP
