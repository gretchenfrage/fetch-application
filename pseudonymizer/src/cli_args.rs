//! Handling of command line arguments.

use std::env::args;


/// Command line argument to this executable.
#[derive(Debug, Clone)]
pub struct CliArgs {
    /// AWS SQS queue URL to pull messages from.
    ///
    /// For example:
    /// `http://localhost:4566/000000000000/login-queue`
    pub queue_url: String,
    /// Postgresql database URL to put anonymized messages into.
    ///
    /// For example:
    /// `postgresql://postgres:postgres@localhost:5433`
    pub postgres_url: String,
}

impl CliArgs {
    /// Attempt to construct from command line arguments. Print a "usage"
    /// message to stderr upon failure.
    pub fn parse_or_print_msg() -> Option<Self> {
        let args = args().collect::<Vec<_>>();
        if args.len() != 3 {
            // TODO: I don't like how parsing logic and interactions with
            //       global process state (argv and stderr) are entangled here
            eprintln!("usage: pseudonymizer [SQS queue URL] [postgres URL]");
            return None;
        }
        Some(CliArgs {
            queue_url: args[1].clone(),
            postgres_url: args[2].clone(),
        })
    }
}
