//! AWS SQS client logic.

use crate::{
    LoginMessage,
    LoginMessageBody,
};
use std::env;
use rusoto_core::{
    Region,
    Client,
    HttpClient,
};
use rusoto_sqs::{
    Sqs,
    SqsClient,
    ReceiveMessageRequest,
};
use chrono::{
    NaiveDate,
    NaiveTime,
    NaiveDateTime,
    Duration,
};
use anyhow::Result;


/// Env var for setting localstack URL.
const LOCALSTACK_URL_VAR: &'static str = "LOCALSTACK_URL";


/// Construct an SQS client fit for connecting to a localstack server (for
/// testing) at the given URL (eg. `http://localhost:4566/`).
fn init_sqs_client_localstack(localstack_url: &str) -> Result<SqsClient> {
    // constructing custom reason necessary to avoid "invalid address"
    // errors. see:
    //
    // https://github.com/rusoto/rusoto/issues/996
    let region = Region::Custom {
        name: "localstack".into(),
        endpoint: localstack_url.into(),
    };

    // construct a non-signing client. localstack ignores authorization checks
    // by default, but we still need to do this to convince rusoto_sqs to send
    // the requests at all.
    let aws_client = Client::new_not_signing(HttpClient::new()?);
    
    Ok(SqsClient::new_with_client(aws_client, region))
}


/// Construct an SQS client based on environment variables.
///
/// If `LOCALSTACK_URL` is set, constructs a client for connecting to that
/// localstack server. Otherwise, configures with standard AWS environment
/// variables. See:
///
/// <https://github.com/rusoto/rusoto/blob/0df121f3c4a23c52b0ef9abcb35e7ff556c63afb/AWS-CREDENTIALS.md>
pub fn init_sqs_client() -> Result<SqsClient> {
    if let Ok(localstack_url) = env::var(LOCALSTACK_URL_VAR) {
        init_sqs_client_localstack(&localstack_url)
    } else {
        Ok(SqsClient::new(Default::default()))
    }
}


/// Attempt to receive a batch of messages from an SQS queue using the given
/// client.
pub async fn receive_messages(
    client: &mut SqsClient,
    queue_url: &str,
) -> Result<Vec<LoginMessage>> {
    // key to be used in the SQS API to refer to the message metadata property
    // denoting the time the message was sent into the queue.
    const SENT_TIMESTAMP_KEY: &'static str = "SentTimestamp";
    
    // this is the maximum number of messages the API allows requesting at a
    // time.
    const MAX_NUMBER_OF_MESSAGES: i64 = 10;

    // the unix epoch
    let epoch = NaiveDateTime::new(
        NaiveDate::from_ymd_opt(1970, 1, 1).unwrap(),
        NaiveTime::from_hms_opt(0, 0, 0).unwrap(),
    );

    // make a request
    let received = client
        .receive_message(ReceiveMessageRequest {
            queue_url: queue_url.into(),
            attribute_names: Some(vec![
                SENT_TIMESTAMP_KEY.into(),
            ]),
            max_number_of_messages: Some(MAX_NUMBER_OF_MESSAGES),
            ..Default::default()
        })
        .await?;

    // convert into our formats
    Ok(received.messages
        .map(|messages| messages.into_iter()
            .map(|message| {
                // parse the body as JSON
                let body = message.body
                    .map(|body|
                        serde_json::from_str::<LoginMessageBody>(&body))
                    .transpose()?
                    // if no body is returned, convert that into a body where
                    // all fields are absent
                    .unwrap_or_else(|| LoginMessageBody {
                        user_id: None,
                        app_version: None,
                        device_type: None,
                        ip: None,
                        locale: None,
                        device_id: None,
                    });

                // the sent timestamp attribute is represented as an integer
                // number of milliseconds since the unix epoch. convert it into
                // a `chrono::NaiveDateTime`.
                let sent_timestamp = message.attributes.as_ref().unwrap()
                    .get(SENT_TIMESTAMP_KEY)
                    .map(|sent_timestamp| sent_timestamp.parse::<i64>())
                    .transpose()?
                    .map(Duration::milliseconds)
                    .map(|sent_timestamp| epoch + sent_timestamp);

                Ok(LoginMessage {
                    body,
                    sent_timestamp,
                })
            })
            .collect::<Result<Vec<_>>>())
        .transpose()?
        .unwrap_or(Vec::new()))
}
