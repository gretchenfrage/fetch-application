//! Pseudonymization procedures.
//!
//! Pseudonymizes fields in a stateless by using a secret AES256 key and the
//! FF1 algorithm, as specified in NIST Special Publication 800-38G, to
//! deterministically encrypt certain fields while preserving certain formats
//! and field sizes that log parsers may assume.

use crate::LoginMessage;
use std::net::IpAddr;
use fpe::ff1::{
    FF1,
    FlexibleNumeralString,
};
use aes::Aes256;
use regex::Regex;
use lazy_static::lazy_static;
use anyhow::{
    Result,
    ensure,
};


/// Pseudonymize all fields of a log message that this executable
/// pseudonymizes, in-place.
pub fn pseudonymize_message(
    key: &[u8; 32],
    message: &mut LoginMessage,
) -> Result<()> {
    if let Some(ref mut ip_addr) = message.body.ip {
        *ip_addr = pseudonymize_ip_addr(key, *ip_addr);
    }
    if let Some(ref mut device_id) = message.body.device_id {
        *device_id = pseudonymize_device_id(key, device_id)?;
    }
    Ok(())
}


/// Given a key and a sequence of arbitrary bytes, use the FF1 algorithm to
/// transform it into some other sequence of arbirary bytes of the same length.
fn pseudonymize_bytes<const N: usize>(
    key: &[u8; 32],
    input: &[u8; N],
) -> [u8; N] {
    // setup
    // unwrap safety: the key is guaranteed on the type-level to be the correct
    //                length, so the unwrap will not fail
    let fpe_ff = FF1::<Aes256>::new(key, 256).unwrap();
    
    // convert into input format
    let enc_input = input.iter().copied()
        .map(|byte| byte as u16)
        .collect::<Vec<u16>>();
    let enc_input = FlexibleNumeralString::from(enc_input);

    // encrypt
    // unwrap safety: so long as we don't misuse the encryption API, this
    //                unwrap won't fail
    let enc_output = fpe_ff.encrypt(&[], &enc_input).unwrap();
    let enc_output = Vec::from(enc_output);
    
    // convert into output format
    let mut output = [0; N];
    for (i, b) in output.iter_mut().enumerate() {
        // unwrap safety: we passed in a radix of 256, so each output value
        //                will be less than 256, so conversion t u8 won't fail
        *b = u8::try_from(enc_output[i]).unwrap();
    }
    output
}


/// Pseudonymize an IP address.
fn pseudonymize_ip_addr(key: &[u8; 32], addr: IpAddr) -> IpAddr {
    // whether we're given an IPV4 or IPV6 address, just encrypt its raw
    // binary representation. this does betray whether the user is connecting
    // from an IPV4 or IPV6 address. however, that is probably fine as this is
    // very little information. furthermore, converting between the two could
    // be undesirable because it may be information relevant to technical
    // failures and thus relevant information to analysts analyzing the logs.
    match addr {
        IpAddr::V4(addr) => IpAddr::V4(pseudonymize_bytes(key, &addr.octets()).into()),
        IpAddr::V6(addr) => IpAddr::V6(pseudonymize_bytes(key, &addr.octets()).into()),
    }
}


/// Pseudonymize a device ID.
fn pseudonymize_device_id(key: &[u8; 32], device_id: &str) -> Result<String> {
    // device IDs are always of a format like 311-31-3355. to pseudonymize
    // it, we remove the hyphens, encrypt the remaining digits (with radix 10),
    // and then re-insert the hyphens.

    // validate format
    lazy_static! {
        static ref DEVICE_ID_REGEX: Regex = Regex::new(r"\d{3}-\d{2}-\d{4}").unwrap();
    }
    ensure!(
        DEVICE_ID_REGEX.is_match(device_id),
        "invalid device id {:?}",
        device_id,
    );

    // TODO: instead of repeating these magic numbers, they should be somehow
    //       structured as values within constant lists, and processes in loops

    // convert into an array of 9 ints in the 0..10 range
    let input_ascii = device_id.as_bytes();

    let mut input_digits = [0; 9];
    input_digits[0..=2].copy_from_slice(&input_ascii[0..=2]);
    input_digits[3..=4].copy_from_slice(&input_ascii[4..=5]);
    input_digits[5..=8].copy_from_slice(&input_ascii[7..=10]);
    for b in &mut input_digits {
        *b -= b'0';
    }

    // init encryption function
    let fpe_ff = FF1::<Aes256>::new(key, 10).unwrap();

    // encrypt
    let enc_input = input_digits.iter().copied()
        .map(|byte| byte as u16)
        .collect::<Vec<u16>>();
    let enc_input = FlexibleNumeralString::from(enc_input);

    // convert output back into an array of ASCII characters
    let enc_output = fpe_ff.encrypt(&[], &enc_input).unwrap();
    let enc_output = Vec::from(enc_output);

    let mut output_digits = [0; 9];
    for (i, b) in output_digits.iter_mut().enumerate() {
        *b = u8::try_from(enc_output[i]).unwrap();
        debug_assert!(*b < 10);
        *b += b'0';
    }

    // copy ranges to a new ASCII array with dashes in it
    let mut output_ascii = [0; 11];
    output_ascii[0..=2].copy_from_slice(&output_digits[0..=2]);
    output_ascii[3] = b'-';
    output_ascii[4..=5].copy_from_slice(&output_digits[3..=4]);
    output_ascii[6] = b'-';
    output_ascii[7..=10].copy_from_slice(&output_digits[5..=8]);

    // done, convert to String
    Ok(String::from_utf8(Vec::from(output_ascii)).unwrap())
}
