//! Utility that pulls login log messages from an AWS SQS queue, pseudonymizes
//! certain fields using the FF1 algorithm, and puts the pseudonymized versions
//! into a postgresql database.


use std::net::IpAddr;
use chrono::NaiveDateTime;
use serde::Deserialize;
use anyhow::Result;

use crate::{
    cli_args::CliArgs,
    key::determine_key,
    sqs_client::{
        init_sqs_client,
        receive_messages,
    },
    sql_client::{
        init_sql_client,
        insert_message,
    },
    pseudonymize::pseudonymize_message,
};


pub mod cli_args;
pub mod key;
pub mod sqs_client;
pub mod sql_client;
pub mod pseudonymize;


/// Semi-structured representation of all relevant information in an individual
/// message.
#[derive(Debug, Clone)]
pub struct LoginMessage {
    /// Deserialized body.
    pub body: LoginMessageBody,
    /// Representation of the `SentTimestamp` SQS attribute.
    pub sent_timestamp: Option<NaiveDateTime>,
}


/// Semi-structured representation of the body part of a `LoginMessage`.
#[derive(Debug, Clone, Deserialize)]
pub struct LoginMessageBody {
    pub user_id: Option<String>,
    pub app_version: Option<String>,
    pub device_type: Option<String>,
    pub ip: Option<IpAddr>,
    pub locale: Option<String>,
    pub device_id: Option<String>,
}


#[tokio::main]
async fn main() -> Result<()> {
    // initialize
    let cli_args = CliArgs::parse_or_print_msg().unwrap();
    let key = determine_key().await?;
    let mut sqs_client = init_sqs_client()?;
    let mut sql_client = init_sql_client(&cli_args.postgres_url).await?;

    // receive messages until the queue is empty
    let mut messages;
    while {
        messages = receive_messages(
            &mut sqs_client,
            &cli_args.queue_url,
        ).await?;
        !messages.is_empty()
    } {
        for mut message in messages {
            // pseudonymize the fields of the message we want to pseudonymize,
            // in-place
            pseudonymize_message(&key, &mut message)?;

            // insert the message into the SQL database
            insert_message(&mut sql_client, &message).await?;
        }
    }

    Ok(())
}
