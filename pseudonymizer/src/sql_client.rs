//! Postgresql client logic.

use crate::LoginMessage;
use tokio_postgres::{
    Client,
    tls::NoTls,
};
use tokio::spawn;
use anyhow::Result;


/// Construct a postgresql client that connects to the database at the given
/// URL.
///
/// For example:
/// `postgresql://postgres:postgres@localhost:5433`
pub async fn init_sql_client(postgresql_url: &str) -> Result<Client> {
    // TODO: support TLS certificates, probably configured via environment
    //       variables

    let (
        client,
        connection,
    ) = tokio_postgres::connect(postgresql_url, NoTls).await?;

    // according to the tokio_postgres docs, we're supposed to spawn the
    // connection on the tokio runtime to drive it
    spawn(async move {
        if let Err(e) = connection.await {
            // TODO: consider using a real logging framework
            eprintln!("postgresql connection error: {}", e);
        }
    });

    Ok(client)
}


/// Using a client, insert a message as-is into the SQL database.
pub async fn insert_message(
    client: &mut Client,
    message: &LoginMessage,
) -> Result<()> {
    // prepare fields that need preparing
    let ip = message.body.ip.map(|ip| ip.to_string());
    let create_date = message.sent_timestamp
        .map(|date_time| date_time.date());

    // the "app_version" fields that have appeared in the JSON are semantic
    // version strings, however, the provided database schema uses an
    // integer type for the app version field. there are several ways we could
    // make this conversion, and we will simply remove any non-decimal
    // characters then parse it as an integer. so, for example, "3.0.4" will
    // get converted to 304. there are several disadvantages to _any_ sort of
    // semver-to-int conversion technique. for example, for this one, there
    // are collisions ("3.12.9" == "3.1.29"), and it copes quite poorly with
    // suffixes ("1.0.2-SNAPSHOT" == "1.0.2"). if this were a real project, I
    // would ask for clarification, and like push to have the database schema
    // use strings for the column.
    let app_version = message.body.app_version.as_ref()
        .map(|string| string.chars()
            .filter(|&c| c >= '0' && c <= '9')
            .collect::<String>()
            .parse::<i32>())
        .transpose()?;

    // TODO: this is enough values that this becomes cumbersome--using a
    //       dedicated SQL query formatting library could be a good idea.
    //       on the other hand, one could argue it would be overkill for such
    //       a one-shot use case. on the other hand, one could argue that
    //       adopting such a tool into a group's domain of familiarity would
    //       pay off in the future. on the other hand, I am not actually
    //       working on this in a team. on the other hand, this is supposed to
    //       be representative of what it would be like for me to be working on
    //       a team. and so on and so forth...
    client
        .execute(
            "INSERT INTO user_logins (
                user_id,
                device_type,
                masked_ip,
                masked_device_id,
                locale,
                app_version,
                create_date
            )
            VALUES ($1, $2, $3, $4, $5, $6, $7);",
            &[
                &message.body.user_id,
                &message.body.device_type,
                &ip,
                &message.body.device_id,
                &message.body.locale,
                &app_version,
                &create_date,
            ]
        )
        .await?;
    Ok(())
}
