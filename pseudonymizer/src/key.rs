//! Handling of the pseudonymization secret key.
//!
//! The pseudonymization secret key is an 32-byte AES256 encryption key, which
//! is used in the encryption of fields that require pseudonymization. The key
//! is stored in base64 format (in STANDARD_NO_PAD) mode in the file
//! `./pseudonymization-secret-key`. If the file does not already exist,
//! openssl can be used to randomly generate a key.

use std::io::ErrorKind;
use tokio::fs;
use base64::{
    Engine,
    engine::general_purpose::STANDARD_NO_PAD,
};
use anyhow::{
    Result,
    anyhow,
};


/// Name of the file we store the base64 AES secret key in.
const KEY_FILE_NAME: &'static str = "pseudonymization-secret-key";


// TODO: creating a type alias or newtype for the key could be good

/// Read the key file if it exists, randomly generate a key and write it to the
/// key file if it doesn't.
pub async fn determine_key() -> Result<[u8; 32]> {
    match fs::read(KEY_FILE_NAME).await {
        Ok(base64) => {
            // file successfully read. decode.
            let bytes = STANDARD_NO_PAD.decode(base64)?;
            bytes.try_into()
                .map_err(|_| anyhow!("wrong length key"))
        }
        Err(e) => {
            if e.kind() == ErrorKind::NotFound {
                // file doesn't exist.

                // use openssl to securely randomly generate a key
                let mut buf = [0; 32];
                openssl::rand::rand_bytes(&mut buf)?;

                // create the file, encode and write the key to it.
                let base64 = STANDARD_NO_PAD.encode(buf);
                fs::write(KEY_FILE_NAME, base64).await?;

                Ok(buf)
            } else {
                // some other error occurred attempting to read the file
                Err(e.into())
            }
        }
    }
}
